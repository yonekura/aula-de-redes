import rsa
import hashlib

(publica, privada) = rsa.newkeys(1024)

BLOCKSIZE = 65536
hasher = hashlib.sha1()
with open('text.txt', 'rb') as afile:
    buf = afile.read(BLOCKSIZE)
    while len(buf) > 0:
        hasher.update(buf)
        mensagem = buf
        buf = afile.read(BLOCKSIZE)
        
cryp = rsa.encrypt(mensagem, publica)

decryp = rsa.decrypt(cryp, privada)

messagemDecryp = hashlib.sha1(decryp)

if (hasher.hexdigest() == messagemDecryp.hexdigest()):
    print "Menssagem integra."
else:
    print "Mensagem foi modificada."

