import socket
import thread
TCP_IP = '127.0.0.1'
TCP_PORT = 8000
BUFFER_SIZE = 20 

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()
print 'Endereço de conexão:', addr

def handle_client(client_socket):
    while True:
        data = client_socket.recv(1024)
        if not data: break
        print('Cliente disse: ' + data)
        print('Enviando para o cliente: ' + data)
        client_socket.send(data)
    client_socket.close()

while True:
    client_socket, addr = s.accept()
    print('Conexao de: '+str(addr))
    thread.start_new_thread(handle_client ,(client_socket,))
s.close()

